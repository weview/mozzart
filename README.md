
# Mozzart

Use it to run, watch and restart more than one app at a time. Very useful when developping a **microservices** app.

```bash
npm install mozzart
```
